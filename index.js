
const path = require('path')
const process = require('process')

class Loader extends Map {
    
    listener = null

    init() {
        this.systemPath = process.cwd()
        this.dashboardsPath = path.join(this.systemPath, 'dashboards')
        this.set('Loader', this)
    }

    process(config){
        // SORT
        this.#event('event', 'Start loading')
        config.sort((prev, next) => {
            return (prev.prioritet > next.prioritet) ? 1 : -1
        })

        // CREATE PLUGINS
        for (const index in config) {
            var acts = config[index].path.split('.')
            var mod = acts.shift()
            const CM = require(mod)
            const CS = this.#getFromPath(acts, CM)
            var plugin = {}
            try{ 
                plugin = new CS()
            }catch(err){
                throw new Error(`Plugin not found in ${config[index].path}`)
            }
            plugin.config = config[index].config
            this.set(config[index].plugin, plugin)
            this.#event('info', `Set plugin ${config[index].plugin}`)
        }
        
        // Check params
        for (const index in config) {
            if (this.get(config[index].plugin).checkParams && this.get(config[index].plugin).validate){
                this.get(config[index].plugin).validate()
                this.#event('info', `Validate plugin ${config[index].plugin}`)
            }
        }

        try {
            // RUN INIT
            for (const index in config) {
                if (this.get(config[index].plugin).init){
                    this.get(config[index].plugin).init()
                    this.#event('info', `Init plugin ${config[index].plugin}`)
                }
            }

            // RUN PROCESS
            for (const index in config) {
                if (this.get(config[index].plugin).process) {
                    this.get(config[index].plugin).process()
                    this.#event('info', `Process plugin ${config[index].plugin}`)
                }
            }
        } catch (err) {
            console.error(err)
            process.exit()
        }
        this.#event('event', 'End loading')
    }


    setListener(cb){
        this.listener = cb
    }

    #event = (status, message) => {
        if (this.listener) this.listener(status,message)
    }

    #getFromPath = (acts, data) => {
        var sdata = data
        var i = 1
        for (var act of acts){
            if (sdata === undefined) 
                throw new Error("Path " + path + " link to undefined var")

            if (acts.length === i) 
                return sdata[act]; 
            else 
                sdata = sdata[act]; i++
        }
    }
}

module.exports = new Loader()